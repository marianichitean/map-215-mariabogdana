package service;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Tuple;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.FriendshipValidator;
import ro.ubbcluj.map.repository.Repository;

import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class ServiceUser<ID,E extends Entity<ID>> {

    private Repository userRepository;
    private Repository friendRepository;
    private FriendshipValidator f =new FriendshipValidator();

    public ServiceUser(Repository userRepository, Repository friendRepository) {
            this.userRepository = userRepository;
            this.friendRepository = friendRepository;
    }

    public E findOneUser(ID id){
        friendUser();
        return (E)userRepository.findOne(id);
    }

    public Iterable<E> findAllUsers(){
        Iterable<E> l=userRepository.findAll();
        friendUser();
        return l;
    }

    public E saveUser(User e) {
        User user = (User) userRepository.save(e);
        return (E) user;
    }

    public E removeUser(ID id){
        User user=(User)userRepository.remove(id);
        userFriend();
        friendUser();
        return (E)user;
    }

    void friendUser(){

        Iterable<User> l1=userRepository.findAll();
        Iterable<Friendship> l2=friendRepository.findAll();
        List<User> us=new LinkedList<>();
        for(User u:l1){
            List<User> rez=new LinkedList<User>();
            for(Friendship f:l2){
                if(f.getU1().getId().equals(u.getId())){
                    rez.add(f.getU2());
                }
                if(f.getU2().getId().equals(u.getId())){
                    rez.add(f.getU1());
                }
            }
            u.setFriendList(rez);
            us.add(u);
        }
        for(User u:us){
            userRepository.update(u);
        }
    }

    void userFriend(){
        Iterable<User> l1=userRepository.findAll();
        Iterable<Friendship> l2=friendRepository.findAll();
        List<Tuple<Long,Long>> list=new LinkedList<>();
        for(Friendship f:l2){
            int nr=0;
            for(User u:l1){
                if(u.getId().equals(f.getU1().getId()) || u.getId().equals(f.getU2().getId()))
                    nr++;
            }
            if(nr!=2){
                Tuple<Long,Long> t=new Tuple<>(f.getU1().getId(),f.getU2().getId());
                list.add(t);
            }

        }
        for(Tuple<Long,Long> t:list){
            friendRepository.remove(t);
        }

    }


    public List<String> getListaPrieteni(Long id){
        HashSet<Friendship> list1 = (HashSet<Friendship>)friendRepository.findAll();
        List<Friendship> list = new ArrayList<>(list1);
        List<String> s = list.stream().filter(x->x.getU1().getId().equals(id) || x.getU2().getId().equals(id))
                .map(y->{
                    if(y.getU1().getId()==id){
                        String st=y.getU2().getFirstName()+" "+y.getU2().getLastName()+" "+y.getDate();
                        return st;
                    }
                    else {
                        String st = y.getU1().getFirstName() + " " + y.getU1().getLastName() + " " + y.getDate();
                        return st;
                    }
                }).toList();
                return s;
    }



    public String getListaPrieteniData(Long id,String luna){
        HashSet<Friendship> hts = (HashSet<Friendship>)friendRepository.findAll();
        List<Friendship> list = new ArrayList<>(hts);
        String s = list.stream().filter(x->x.getU1().getId().equals(id) || x.getU2().getId().equals(id))
                .map(y->{
                    if(y.getU1().getId()==id){
                        String st=y.getU2().getFirstName()+" "+y.getU2().getLastName()+" "+(y.getDate().getMonth()+1);
                        return st;
                    }
                    else {
                        String st = y.getU1().getFirstName() + " " + y.getU1().getLastName() + " " + (y.getDate().getMonth()+1);
                        return st;
                    }
                })
                .reduce("",(a,b)->{
                    if(b.endsWith(luna))
                        a=a+b+";";
                    return a;
                });
        return s;
    }




}
