package service;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Tuple;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.FriendshipValidator;
import ro.ubbcluj.map.repository.Repository;

import java.util.LinkedList;
import java.util.List;

public class ServiceFriendship<ID,E extends Entity<ID>> {

    private Repository userRepository;
    private Repository friendRepository;
    private FriendshipValidator f =new FriendshipValidator();

    public ServiceFriendship(Repository userRepository, Repository friendRepository) {
        this.userRepository = userRepository;
        this.friendRepository = friendRepository;
    }

    public E findOneFriendship(ID id){
        userFriend();
        return (E)friendRepository.findOne(id);
    }

    public Iterable<E> findAllFriendships(){
        Iterable<E> t= friendRepository.findAll();
        userFriend();
        return t;
    }

    public E saveFriend(Friendship e) {
        Iterable<User> l=(Iterable<User>)userRepository.findAll();
        f.validate1(e,l);
        Friendship friendship = (Friendship) friendRepository.save(e);
        User u1=e.getU1();
        User u2=e.getU2();
        u1.addFriend(u2);
        u2.addFriend(u1);
        userRepository.update(u1);
        userRepository.update(u2);
        //friendUser();
        return (E) friendship;
    }

    public E removeFriend(ID id){
        Friendship friendship = (Friendship) friendRepository.remove(id);
        if(friendship!=null){
            User u1 = friendship.getU1();
            User u2 = friendship.getU2();
            u1.removeFriend(u2);
            u2.removeFriend(u1);
            userRepository.update(u1);
        }

        friendUser();
        return (E)friendship;
    }

    void friendUser(){

        Iterable<User> l1=userRepository.findAll();
        Iterable<Friendship> l2=friendRepository.findAll();
        List<User> us=new LinkedList<>();
        for(User u:l1){
            List<User> rez=new LinkedList<User>();
            for(Friendship f:l2){
                if(f.getU1().getId().equals(u.getId())){
                    rez.add(f.getU2());
                }
                if(f.getU2().getId().equals(u.getId())){
                    rez.add(f.getU1());
                }
            }
            u.setFriendList(rez);
            us.add(u);
        }
        for(User u:us){
            userRepository.update(u);
        }
    }

    void userFriend(){
        Iterable<User> l1=userRepository.findAll();
        Iterable<Friendship> l2=friendRepository.findAll();
        List<Tuple<Long,Long>> list=new LinkedList<>();
        for(Friendship f:l2){
            int nr=0;
            for(User u:l1){
                if(u.getId().equals(f.getU1().getId()) || u.getId().equals(f.getU2().getId()))
                    nr++;
            }
            if(nr!=2){
                Tuple<Long,Long> t=new Tuple<>(f.getU1().getId(),f.getU2().getId());
                list.add(t);
            }

        }
        for(Tuple<Long,Long> t:list){
            friendRepository.remove(t);
        }

    }

}
