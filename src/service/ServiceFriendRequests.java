package service;

import ro.ubbcluj.map.domain.*;
import ro.ubbcluj.map.repository.Repository;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Date;

public class ServiceFriendRequests<ID,E extends Entity<ID>>{
    private Repository friendRequestRepository;
    private Repository friendshipRepository;

    public ServiceFriendRequests(Repository friendRequestRepository, Repository friendshipRepository) {
        this.friendRequestRepository = friendRequestRepository;
        this.friendshipRepository = friendshipRepository;
    }

    public E findOneFriendRequest(ID id){
        return (E)friendRequestRepository.findOne(id);
    }

    public Iterable<E> findAllFriendRequests(){
        Iterable<E> l=friendRequestRepository.findAll();
        return l;
    }

    public E saveFrienRequest(FriendRequest e) {
        FriendRequest friendRequest = (FriendRequest)friendRequestRepository.save(e);
        return (E) friendRequest;
    }

    public E removeFriendRequest(ID id){
        FriendRequest friendRequest=(FriendRequest) friendRequestRepository.remove(id);
        return (E)friendRequest;
    }

    public E updateFriendRequest(FriendRequest f){
        if(f.getStatus().equals("approved")){
            Friendship friendship = new Friendship(f.getUser1(),f.getUser2());
            Tuple<Long, Long> t = new Tuple<>(f.getUser1().getId(), f.getUser2().getId());
            friendship.setId(t);
            Date date = new Date();
            Timestamp time = new Timestamp(date.getTime());
            friendship.setDate(time);
            friendshipRepository.save(friendship);
        }
        FriendRequest fr = (FriendRequest) friendRequestRepository.update(f);
        return (E)fr;
    }

}
