package service;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.Message;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.repository.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public class ServiceMessage<ID,E extends Entity<ID>>{
    private Repository messageRepository;

    public ServiceMessage(Repository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public E findOneMessage(ID id){
        return (E)messageRepository.findOne(id);
    }

    public Iterable<E> findAllMessages(){
        Iterable<E> l=messageRepository.findAll();
        return l;
    }

    public E saveMessage(Message e) {
        Message message = (Message) messageRepository.save(e);
        return (E) message;
    }

    public E removeMessage(ID id){
        Message message=(Message) messageRepository.remove(id);
        return (E)message;
    }

    public List<Message> getReceivedMessages(User user){
        HashSet<Message> hts = (HashSet<Message>)messageRepository.findAll();
        List<Message> list = new ArrayList<>(hts);
        List<Message> s = list.stream().filter(x->x.getTo().contains(user)).toList();
        return s;
    }

    public List<Message> getsentMessages(User user){
        HashSet<Message> hts = (HashSet<Message>)messageRepository.findAll();
        List<Message> list = new ArrayList<>(hts);
        List<Message> s = list.stream().filter(x->x.getFrom().equals(user)).toList();
        return s;
    }

    public List<Message> getSortedMessages(User u1,User u2){
        List<Message> lstSentU1 = new ArrayList<>();
        List<Message> lstSentU2 = new ArrayList<>();
        lstSentU1 = getsentMessages(u1).stream().filter(x->x.getTo().contains(u2)).toList();
        lstSentU2 = getsentMessages(u2).stream().filter(x->x.getTo().contains(u1)).toList();
        List<Message> list = new ArrayList<>();
        list.addAll(lstSentU1);
        list.addAll(lstSentU2);
        list.sort(Comparator.comparing(o -> o.getData()));
        return list;
    }
}
