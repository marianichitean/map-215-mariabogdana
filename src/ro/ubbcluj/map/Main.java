package ro.ubbcluj.map;

import ro.ubbcluj.map.domain.*;
import ro.ubbcluj.map.model.validators.FriendRequestValidator;
import ro.ubbcluj.map.model.validators.FriendshipValidator;
import ro.ubbcluj.map.model.validators.MessageValidator;
import ro.ubbcluj.map.model.validators.UserValidator;
import ro.ubbcluj.map.repository.database.FriendRequestDB;
import ro.ubbcluj.map.repository.database.FriendshipDB;
import ro.ubbcluj.map.repository.database.MessageDB;
import ro.ubbcluj.map.repository.database.UserDB;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.ui.Options;
import ro.ubbcluj.map.utils.Graf;
import service.*;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) throws FileNotFoundException {

        //Repository<Long, User> repo1 = new UserFile("C:\\Users\\user\\Desktop\\curs3_generics\\src\\testUser.in", new UserValidator());

        //Repository<Tuple<Long, Long>, Friendship> repo = new FriendFile("C:\\Users\\user\\Desktop\\curs3_generics\\src\\testFriendship.in", new FriendshipValidator());

        //Repository<Long, User> repo1 = new InMemoryRepository<>(new UserValidator());

        //Repository<Tuple<Long,Long>, Friendship> repo=new InMemoryRepository<>(new FriendshipValidator());

        Repository<Long,User> repo1 = new UserDB("jdbc:postgresql://localhost:5432/retea","postgres","Metodeavansate10",new UserValidator());
        Repository<Tuple<Long,Long>,Friendship> repo = new FriendshipDB("jdbc:postgresql://localhost:5432/retea","postgres","Metodeavansate10",new FriendshipValidator());
        Repository<Long, Message> repo2 = new MessageDB("jdbc:postgresql://localhost:5432/retea","postgres","Metodeavansate10",new MessageValidator());
        Repository<Long, FriendRequest> repo3 = new FriendRequestDB("jdbc:postgresql://localhost:5432/retea","postgres","Metodeavansate10", new FriendRequestValidator());
        repo1.findAll().forEach(System.out::println);
        repo.findAll().forEach(System.out::println);
        ServiceUser serviceUser = new ServiceUser(repo1,repo);
        ServiceFriendship serviceFriendship = new ServiceFriendship(repo1,repo);
        ServiceMessage serviceMessage = new ServiceMessage(repo2);
        ServiceFriendRequests serviceFriendRequests = new ServiceFriendRequests(repo3,repo);
        Options optiuni=new Options(serviceUser,serviceFriendship,serviceMessage,serviceFriendRequests);
        optiuni.optiuni();


        int[][] a=new int[150][150];
        Map<Integer,Long> map = new HashMap<>();
        int n=0;
        Iterable<User> l2=serviceUser.findAllUsers();
        Iterable<Friendship> l1=serviceFriendship.findAllFriendships();
        int i=0,j;
        for(User u:l2) {
            n++;
            map.put(i,u.getId());
            j = 0;
            for (User u1 : l2) {
                for (Friendship f : l1) {
                    if ((f.getU1().getId().equals(u.getId()) && f.getU2().getId().equals(u1.getId()))||(f.getU1().getId().equals(u1.getId()) && f.getU2().getId().equals(u.getId())))
                    {a[i][j] = 1;
                    }


                }
                j++;
            }
            i++;}

        int maxx=0;
        Graf c=new Graf(a,n,maxx);
        System.out.println("Numarul de componente conexe e "+c.fin(map));
        System.out.println("Lungimea celei mai lungi comunitati "+c.getMaxim());

    }
}