package ro.ubbcluj.map.repository.memory;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository<ID, E extends Entity<ID>> implements Repository<ID, E> {
    private Map<ID, E> entities;
    private Validator<E> validator;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    @Override
    public E findOne(ID id) {
        if(id == null)
            throw new IllegalArgumentException("id must not be null");

        var wrapper = new Object() { E entity = null; };
        entities.forEach((ID,e) ->{
            if(e.getId().equals(id))
                wrapper.entity=e;
        });
        return wrapper.entity;

    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) {
        if(entity == null)
            throw new ValidationException("Entity must not be null");
        validator.validate(entity);
        //if(entities.get(entity.getId())!=null)
          //  return entity;
        if(findOne(entity.getId())!=null)
            return entity;
        entities.put(entity.getId(),entity);
        return null;
    }

    @Override
    public E remove(ID id) {
        if(id==null){
            throw new IllegalArgumentException("Id ul nu trebuie sa fie null");
        }
        if(entities.get(id) == null) {
            return null;
        }
        E entity=findOne(id);
        entities.remove(id);
        return entity;
    }

    @Override
    public E update(E entity) {
        if(entity==null){
            throw new IllegalArgumentException("Entitatea nu trebuie sa fie nula");
    }
    validator.validate(entity);
    if(entities.get(entity.getId()) == null){
        return entity;
    }
    entities.replace(entity.getId(),entity);
    return null;
    }

    public void clear(){
        entities.clear();
    }

    public void setEntities(Map<ID, E> entities) {
        this.entities = entities;
    }
}
