package ro.ubbcluj.map.repository.database;

import ro.ubbcluj.map.domain.Message;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.sql.*;
import java.util.*;

public class MessageDB implements Repository<Long, Message> {
    private String url;
    private String username;
    private String password;
    private Validator<Message> validator;

    public MessageDB(String url, String username, String password, Validator<Message> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public List<String> stringToList(String to){
        List<String> listIdUser = new ArrayList<String>(Arrays.asList(to.split(";")));
        return listIdUser;
    }

    public User getUserById(Long id){
        String sql ="select * from users where id=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setLong(1,id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                User utilizator = new User(firstName, lastName);
                utilizator.setId(id1);
                return utilizator;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<User> getToUserList(String list){
        List<String> listId = stringToList(list);
        List<User> listUser = new ArrayList<>();
        for(String s:listId){
            listUser.add(getUserById(Long.parseLong(s)));
        }
        return listUser;
    }

    public String getStringFromList(List<User> user){
        String stringWithUserId = "";
        for(User u:user){
            stringWithUserId=stringWithUserId + u.getId().toString() + ";";
        }
        return stringWithUserId;
    }

    @Override
    public Message findOne(Long id) {

        if(id == null)
            throw new IllegalArgumentException("id must not be null");
        String sql ="select * from messages where id_m=?";
        String sql1 = "select * from users where id=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setLong(1,id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long idM = resultSet.getLong("id_m");
                Long idF = resultSet.getLong("from_u");
                String to = resultSet.getString("to_u");
                String mesaj = resultSet.getString("message");
                Timestamp data = resultSet.getTimestamp("data");
                Message message = new Message(getUserById(idF),getToUserList(to),mesaj,data);
                message.setId(idM);
                return message;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Iterable<Message> findAll() {
        Set<Message> messages = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from messages");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long idM = resultSet.getLong("id_m");
                Long idF = resultSet.getLong("from_u");
                String to = resultSet.getString("to_u");
                String mesaj = resultSet.getString("message");
                Timestamp data = resultSet.getTimestamp("data");
                Long idR = resultSet.getLong("reply_message");
                Message message = new Message(getUserById(idF),getToUserList(to),mesaj,data);
                message.setId(idM);
                message.setReplyMessage(findOne(idR));
                messages.add(message);
            }
            return messages;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return messages;
    }

    @Override
    public Message save(Message entity) {
        if(entity==null)
            throw new IllegalArgumentException("Mesajul ul nu poate fi null");
        validator.validate(entity);
        if(findOne(entity.getId())!=null)
            return entity;
        String sql = "insert into messages (from_u,to_u,message,data,reply_message) values (?,?, ?, ?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getFrom().getId());
            ps.setString(2, getStringFromList(entity.getTo()));
            ps.setString(3,entity.getMessage());
            Timestamp date = new Timestamp(entity.getData().getTime());
            ps.setTimestamp(4, date);
            if(entity.getReplyMessage()!=null)
               ps.setLong(5,entity.getReplyMessage().getId());
            else
                ps.setLong(5,0L);
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Message remove(Long aLong) {
        if(aLong==null)
            throw new IllegalArgumentException("id-ul nu poate fi null");

        Message m=findOne(aLong);
        if(m==null)
            return null;
        String sql = "delete from messages where id_m = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, aLong);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return m;
    }

    @Override
    public Message update(Message entity) {
        return null;
    }
}
