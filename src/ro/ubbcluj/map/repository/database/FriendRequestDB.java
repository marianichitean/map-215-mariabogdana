package ro.ubbcluj.map.repository.database;

import ro.ubbcluj.map.domain.*;
import ro.ubbcluj.map.model.validators.FriendRequestValidator;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class FriendRequestDB implements Repository<Long, FriendRequest> {

    private String url;
    private String username;
    private String password;
    private Validator<FriendRequest> validator;

    public FriendRequestDB(String url, String username, String password, Validator<FriendRequest> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    public User getUserById(Long id){
        String sql ="select * from users where id=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)){
            statement.setLong(1,id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                User utilizator = new User(firstName, lastName);
                utilizator.setId(id1);
                return utilizator;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest findOne(Long id) {
        if (id == null)
            throw new IllegalArgumentException("id must not be null");

        String sql = "select * from friendrequests where id_fr=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id_fr");
                Long id2 = resultSet.getLong("id_user1");
                Long id3 = resultSet.getLong("id_user2");
                String status = resultSet.getString("status");
                FriendRequest f = new FriendRequest(getUserById(id2),getUserById(id3),status);
                f.setId(id1);
                return f;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Iterable<FriendRequest> findAll() {
        Set<FriendRequest> friendRequests = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendrequests");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id_fr");
                Long id2 = resultSet.getLong("id_user1");
                Long id3 = resultSet.getLong("id_user2");
                String status = resultSet.getString("status");
                FriendRequest f = new FriendRequest(getUserById(id2),getUserById(id3),status);
                f.setId(id1);
                friendRequests.add(f);
            }
            return friendRequests;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendRequests;
    }

    @Override
    public FriendRequest save(FriendRequest entity) {
        if(entity==null)
            throw new IllegalArgumentException("User ul nu poate fi null");
        validator.validate(entity);
        String sql = "insert into friendrequests (id_user1,id_user2,status) values (? , ?, ?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getUser1().getId());
            ps.setLong(2, entity.getUser2().getId());
            ps.setString(3, entity.getStatus());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public FriendRequest remove(Long aLong) {
        if(aLong==null)
            throw new IllegalArgumentException("id ul nu poate fi null");

        FriendRequest f=findOne(aLong);
        if(f==null)
            return null;

        String sql = "delete from friendrequests where id_fr = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, aLong);
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return f;
    }

    @Override
    public FriendRequest update(FriendRequest entity) {
        if(entity==null)
            throw new IllegalArgumentException("User ul nu poae fi null");
        //validator.validate(entity);
        String sql = "update friendrequests set status = ? where id_fr = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1,entity.getStatus());
            ps.setLong(2, entity.getId());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }
}
