package ro.ubbcluj.map.repository.database;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Tuple;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.Repository;

import java.sql.*;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDB implements Repository<Tuple<Long,Long>, Friendship> {

    private String url;
    private String username;
    private String password;
    private Validator<Friendship> validator;

    public FriendshipDB(String url, String username, String password, Validator<Friendship> validator) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.validator = validator;
    }

    @Override
    public Friendship findOne(Tuple<Long, Long> id) {
        if (id == null)
            throw new IllegalArgumentException("id must not be null");

        String sql = "select * from friendships where id1=? and id2=?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id.getE1());
            statement.setLong(2, id.getE2());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                String firstName1 = resultSet.getString("firstname_user1");
                String lastName1 = resultSet.getString("lastname_user1");
                User u1 = new User(firstName1, lastName1);
                u1.setId(id1);
                Long id2 = resultSet.getLong("id2");
                String firstName2 = resultSet.getString("firstname_user2");
                String lastName2 = resultSet.getString("lastname_user2");
                User u2 = new User(firstName2, lastName2);
                u2.setId(id2);
                Timestamp date = resultSet.getTimestamp("data");
                Friendship f = new Friendship(u1, u2);
                f.setId(id);
                f.setDate(date);
                return f;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("SELECT * from friendships");
             ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                Long id1 = resultSet.getLong("id1");
                String firstName1 = resultSet.getString("firstname_user1");
                String lastName1 = resultSet.getString("lastname_user1");
                User u1 = new User(firstName1, lastName1);
                u1.setId(id1);
                Long id2 = resultSet.getLong("id2");
                String firstName2 = resultSet.getString("firstname_user2");
                String lastName2 = resultSet.getString("lastname_user2");
                User u2 = new User(firstName2, lastName2);
                u2.setId(id2);
                Friendship f = new Friendship(u1,u2);
                Timestamp date = resultSet.getTimestamp("data");
                f.setId(new Tuple<Long,Long>(id1,id2));
                f.setDate(date);
                friendships.add(f);
            }
            return friendships;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return friendships;
    }

    @Override
    public Friendship save(Friendship entity) {
        if(entity==null)
            throw new IllegalArgumentException("User ul nu poate fi null");
        validator.validate(entity);
        if(findOne(entity.getId())!=null)
            return entity;

        String sql = "insert into friendships (id1,firstname_user1,lastname_user1,id2,firstname_user2,lastname_user2,data) values (?, ? , ?, ?, ?, ?,?)";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, entity.getU1().getId());
            ps.setString(2, entity.getU1().getFirstName());
            ps.setString(3, entity.getU1().getLastName());
            ps.setLong(4,entity.getU2().getId());
            ps.setString(5,entity.getU2().getFirstName());
            ps.setString(6,entity.getU2().getLastName());
            System.out.println(entity.getDate());
            ps.setTimestamp(7,entity.getDate());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Friendship remove(Tuple<Long,Long> aLong) {
        if(aLong==null)
            throw new IllegalArgumentException("id ul nu poate fi null");

        Friendship f=findOne(aLong);
        if(f==null)
            return null;

        String sql = "delete from friendships where id1 = ? and id2 = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, aLong.getE1());
            ps.setLong(2,aLong.getE2());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return f;
    }

    @Override
    public Friendship update(Friendship entity) {
        if(entity==null)
            throw new IllegalArgumentException("User ul nu poae fi null");
        validator.validate(entity);
        String sql = "update friendships set firstname_user1 = ?, lastname_user1 = ?,firstname_user2 = ?, lastname_user2 = ?  where id1 = ? and id2 = ?";
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(5, entity.getU1().getId());
            ps.setString(1, entity.getU1().getFirstName());
            ps.setString(2, entity.getU1().getLastName());
            ps.setLong(4,entity.getU2().getId());
            ps.setString(6,entity.getU2().getFirstName());
            ps.setString(4,entity.getU2().getLastName());
            ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return entity;
    }

}
