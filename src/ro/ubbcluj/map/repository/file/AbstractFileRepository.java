package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.Entity;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.Validator;
import ro.ubbcluj.map.repository.memory.InMemoryRepository;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractFileRepository<ID,E extends Entity<ID>> extends InMemoryRepository<ID,E> {

    private String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        this.loadData();
    }

    protected abstract String createEntityMyString(E entity);

    protected abstract E extractEntity(List<String> attributes);

    protected void writeToFile(E entity) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,true))) {
            bufferedWriter.write(createEntityMyString(entity));
            bufferedWriter.newLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        super.clear();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(this.fileName))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                List<String> args = Arrays.asList(line.split(";"));
                E entity = extractEntity(args);
                super.save(entity);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*
        Path path= Paths.get(fileName);
        try{
            List<String> lines= Files.readAllLines(path);
            lines.forEach(line->{
                E entity=extractEntity(Arrays.asList(line.split(";")));
                super.save(entity);
            });
    }
        catch(IOException e){
            e.printStackTrace();
        }
        */

    }

    /**
     * Function to get all entities from file
     * @return all entities from file
     */
    public Iterable<E> findAll() {
        loadData();
        return super.findAll();
    }

    public void writeAllToFile(){
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName,false))){
           super.findAll().forEach(entity->{
               try{
                   bufferedWriter.write(createEntityMyString(entity));
                   bufferedWriter.newLine();
               }
               catch(IOException e){
                   e.printStackTrace();
               }
           });
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public E save(E entity) {
        E e = super.save(entity);
        if (e == null) {
            writeToFile(entity);
        }
        return e;
    }

    @Override
    public E update(E entity){
        E e = super.update(entity);
        if(e == null){
            writeAllToFile();
        }
        return entity;
    }

    @Override
    public E remove(ID id){
        E e = super.remove(id);
        if(e != null){
            writeAllToFile();
        }
        return e;
    }
}