package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.Validator;

import java.util.List;

public class UserFile extends AbstractFileRepository<Long, User>{

    public UserFile(String fileName, Validator<User> validator) {
        super(fileName, validator);

    }

    @Override
    protected String createEntityMyString(User entity) {
        return entity.getId()+";"+entity.getFirstName()+";"+entity.getLastName();
    }

    @Override
    protected User extractEntity(List<String> attributes) {
        User user = new User(attributes.get(1),attributes.get(2));
        user.setId(Long.parseLong(attributes.get(0)));
        return user;
    }
}
