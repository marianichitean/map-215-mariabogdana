package ro.ubbcluj.map.repository.file;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.Tuple;
import ro.ubbcluj.map.domain.User;
import ro.ubbcluj.map.model.validators.Validator;

import java.util.List;

public class FriendFile extends AbstractFileRepository<Tuple<Long,Long>, Friendship>{

    public FriendFile(String fileName, Validator<Friendship> validator) {
        super(fileName, validator);
    }

    @Override
    protected String createEntityMyString(Friendship entity) {
        return entity.getU1().getId()+";"+entity.getU1().getFirstName()+";"+entity.getU1().getLastName()+";"+entity.getU2().getId()+";"+entity.getU2().getFirstName()+";"+entity.getU2().getLastName();
    }

    @Override
    protected Friendship extractEntity(List<String> attributes) {
        User user1 = new User(attributes.get(1),attributes.get(2));
        user1.setId(Long.parseLong(attributes.get(0)));

        User user2 = new User(attributes.get(4),attributes.get(5));
        user2.setId(Long.parseLong(attributes.get(3)));

        Friendship f1=new Friendship(user1,user2);
        Tuple<Long,Long> t= new Tuple<>(user1.getId(),user2.getId());
        f1.setId(t);

        return f1;

    }
}