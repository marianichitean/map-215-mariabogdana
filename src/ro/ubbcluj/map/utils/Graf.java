package ro.ubbcluj.map.utils;

import java.util.Map;

public class Graf {

    int[][]mtxAd;
    int n;
    int maxim;
    int[] viz=new int[100];

    public Graf(int[][] mtAd, int n,int maxim) {
        this.mtxAd = mtAd;
        this.n = n;
        this.maxim=maxim;
    }

    public void dfs(int nodStart,int[][] mtxAd,int n,int[] vec, int[]p){
        //marcam nodul de start ca fiind vizitat
        //la urmatoarele cautari nodStart va fi urmatorul nod
        viz[nodStart] = 1;

        //adaugam nodul de start in vector
        //la urmatoarele cautari nodStart va fi celelalte noduri
        //vec[dimVec] = nodStart;
        vec[p[0]] = nodStart;
        //actualizam dimensiunea vectorului
        //dimVec += 1;
        //p[0]=dimVec;
        p[0]=p[0]+1;

        int i;
        //parcurgem linia nodului in matricea de adiacenta
        for(i = 0; i < n; i++){
            //daca este adiacent cu nodul i
            //si nodul i nu a fost vizitat
            if(mtxAd[nodStart][i] == 1 && viz[i]==0){
                //pornim o noua cautare cu nodul i
                dfs(i, mtxAd, n, vec, p);
            }
        }
        //return dimVec;
    }

    public int rezolva(int n,int[][] mtxAd,int[] g){
        //declaram vectorul unde vor fi nodurile din parcurgerea DFS
        //mai precis, va fi introdusa fiecare componenta conexa
        int[] vec=new int[1000];
        int nr=0;
        //dimensiunea vectorului
        int dimVec = 0;
        int dimVec1=0;
        int[]p=new int[2];
        p[0]=0;
        int i;
        //int[] g=new int[100];
        //parcurgem fiecare nod al grafului
        for(i = 0; i < n; i++){
            //reinitializam dimensiunea vectorului
            //pentru a scrie fiecare componenta conexa in vector
            dimVec = 0;


            //daca nodul i nu a fost vizitat
            if(viz[i] == 0){
                //apelam functia dfs
                p[0]=0;
                dfs(i, mtxAd, n, vec,p);
                dimVec1=p[0];
                nr++;
            }
            //verificam daca s-a mai gasit
            //o componenta conexa
            if(dimVec1 > maxim){
                maxim=dimVec1;
                for(int j = 0; j < dimVec1; j++)
                    g[j]=vec[j];

            }

        }
        //for(int j = 0; j < maxim; j++)
            //System.out.println(g[j]+1);
        return nr;
    }

    public int getMaxim() {
        return maxim-1;

    }

    public int fin(Map<Integer,Long> map){
        int o;
        for(o=0;o<100;o++)
            viz[o]=0;
        int[]g=new int[100];
        int i=rezolva(n,mtxAd,g);
        for(int j = 0; j < maxim; j++)
            System.out.println(map.get(g[j]));
        return i;
    }


}