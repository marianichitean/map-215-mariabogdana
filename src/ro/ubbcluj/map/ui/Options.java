package ro.ubbcluj.map.ui;

import ro.ubbcluj.map.domain.*;
import ro.ubbcluj.map.model.validators.InputValidator;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.ValidationExceptionF;
import service.*;
import service.ServiceFriendRequests;
import java.sql.Timestamp;
import java.util.*;

public class Options {

    ServiceUser serviceUser;
    ServiceFriendship serviceFriendship;
    ServiceMessage serviceMessage;
    ServiceFriendRequests serviceFriendRequests;
    public Options(ServiceUser serviceUser, ServiceFriendship serviceFriendship,ServiceMessage serviceMessage,ServiceFriendRequests serviceFriendRequests) {
        this.serviceUser = serviceUser;
        this.serviceFriendship = serviceFriendship;
        this.serviceMessage = serviceMessage;
        this.serviceFriendRequests = serviceFriendRequests;
    }

    public void optiuni() {
        Scanner in1;
        System.out.println("Introduceti o optiune");
        Scanner in = new Scanner(System.in);
        int o = in.nextInt();
        while (o != 0) {
            if (o == 1) {
                //adauga user
                Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
                System.out.println("Numele=");
                in1 = new Scanner(System.in);
                String nume = in1.nextLine();
                System.out.println("Prenume=");
                in1 = new Scanner(System.in);
                String prenume = in1.nextLine();
                User user = new User(nume, prenume);
                user.setId(id);
                try {
                    Entity e = serviceUser.saveUser(user);
                    if (e == null)
                        System.out.println("S-a adaugat utilizatorul");
                    else
                        System.out.println("Deja exista");
                } catch (ValidationException ex) {
                    System.out.println(ex);
                }
            }
            if (o == 2) {
                //elimina user
                System.out.println("ID-ul user-ului");
                in1 = new Scanner(System.in);
                Long id = in1.nextLong();
                try {
                    Entity entity = serviceUser.removeUser(id);
                    if (entity != null) {
                        System.out.println("S-a sters utilizatorul");
                    } else
                        System.out.println("Nu exista utilizatorul cu id-ul dat");
                } catch (IllegalArgumentException ex) {
                    System.out.println(ex);
                }

            }
            if (o == 3) {
                //adauga prieten
                System.out.println("ID-ul primului user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("Numele primului user");
                in1 = new Scanner(System.in);
                String nume1 = in1.nextLine();
                System.out.println("Prenumele primului user");
                in1 = new Scanner(System.in);
                String prenume1 = in1.nextLine();
                System.out.println("ID-ul celui de al doilea user");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                System.out.println("Numele celui de al doilea user");
                in1 = new Scanner(System.in);
                String nume2 = in1.nextLine();
                System.out.println("Prenumele celui de al doilea user");
                in1 = new Scanner(System.in);
                String prenume2 = in1.nextLine();
                User u1 = new User(nume1, prenume1);
                u1.setId(id1);
                User u2 = new User(nume2, prenume2);
                u2.setId(id2);
                Friendship f = new Friendship(u1, u2);
                Tuple<Long, Long> t = new Tuple<>(u1.getId(), u2.getId());
                f.setId(t);
                Date dt = new Date();
                Timestamp date = new Timestamp(dt.getTime());
                f.setDate(date);
                try {
                    Entity e = serviceFriendship.saveFriend(f);
                    if (e == null)
                        System.out.println("S-a adaugat cu succes");
                    else
                        System.out.println("Deja exista");
                } catch (ValidationException ex) {
                    System.out.println(ex);
                } catch (ValidationExceptionF ex) {
                    System.out.println(ex);
                }
            }
            if (o == 4) {
                //sterg prieten
                System.out.println("ID-ul primului user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("ID-ul celui de al doilea user");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                Tuple<Long, Long> t = new Tuple<>(id1, id2);
                //System.out.println(t);
                try {

                    Entity entity = serviceFriendship.removeFriend(t);
                    if (entity != null) {
                        System.out.println("S-a sters cu succes");
                    } else
                        System.out.println("Nu exista utilizatorul cu id-ul dat");
                } catch (IllegalArgumentException ex) {
                    System.out.println(ex);
                }

            }
            if (o == 5) {
                System.out.println(serviceFriendship.findAllFriendships());
            }
            if (o == 6) {
                //System.out.println(service.findAllUsers());

                Iterable<User> l1 = serviceUser.findAllUsers();
                for (User u : l1) {
                    System.out.println(u.getId() + u.getFirstName() + u.getLastName());
                }
            }
            if (o == 7) {
                /*
                System.out.println("ID user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("Nume user");
                in1 = new Scanner(System.in);
                String nume1 = in1.nextLine();
                System.out.println("Prenume user");
                in1 = new Scanner(System.in);
                String prenume1 = in1.nextLine();
                User u1 = new User(nume1, prenume1);
                u1.setId(id1);
                serviceUser.update(u1);
                Iterable<User> l1 = serviceUser.findAllUsers();
                for (User u : l1) {
                    System.out.println(u.getId() + u.getFirstName() + u.getLastName() + u.getFriendList());
                 }
                 */
                }
            if(o==8){
                System.out.println("ID user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                serviceUser.getListaPrieteni(id1).forEach(x->{
                    System.out.println(x);
                });
            }
            if(o==9){
                System.out.println("ID user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("Luna");
                in1 = new Scanner(System.in);
                String luna = in1.nextLine();
                System.out.println(serviceUser.getListaPrieteniData(id1,luna));
            }
            if(o==11){
                System.out.println("ID user");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println(serviceUser.findOneUser(id1));

            }
            if(o==12){
                System.out.println("ID user1");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("ID user2");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                Tuple<Long,Long> tpl=new Tuple<>(id1,id2);
                System.out.println(serviceFriendship.findOneFriendship(tpl));

            }
            if(o==13){
                Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
                System.out.println("ID expeditor");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("Lista destinatari");
                in1 = new Scanner(System.in);
                String st = in1.nextLine();
                System.out.println("Mesaj");
                in1 = new Scanner(System.in);
                String msg = in1.nextLine();
                Date dt = new Date();
                Timestamp date = new Timestamp(dt.getTime());
                System.out.println("ID ul mesajului la care se raspunde/0 daca nu se raspunde");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                InputValidator inputValidator = new InputValidator();
                try{
                    inputValidator.validate(st);
                    Message message = new Message((User)serviceUser.findOneUser(id1),getToUserList(st),msg,date);
                    message.setId(id);
                    message.setReplyMessage((Message) serviceMessage.findOneMessage(id2));
                    try {
                        Entity e =serviceMessage.saveMessage(message);;
                        if (e == null)
                            System.out.println("S-a adaugat mesajul");
                        else
                            System.out.println("Deja exista");
                    } catch (ValidationException ex) {
                        System.out.println(ex);
                    }

                }
                catch (ValidationException e){
                    System.out.println(e);
                }

            }

            if(o==14){
                System.out.println("ID mesaj");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                try {

                    Entity entity = serviceMessage.removeMessage(id1);
                    if (entity != null) {
                        System.out.println("S-a sters cu succes");
                    } else
                        System.out.println("Nu exista utilizatorul cu id-ul dat");
                } catch (IllegalArgumentException ex) {
                    System.out.println(ex);
                }

            }
            if(o==15){
                System.out.println(serviceMessage.findAllMessages());
            }
            if(o==16){
                System.out.println("ID user1");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("ID user2");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                if(serviceUser.findOneUser(id1) == null || serviceUser.findOneUser(id2) == null)
                    System.out.println("Nu exista utilizatorii dati");
                else{
                    List<Message> lst = serviceMessage.getSortedMessages((User)serviceUser.findOneUser(id1),(User)serviceUser.findOneUser(id2));
                    lst.forEach(x->{
                        System.out.println(x);
                    });}
            }
            if(o==17){
                Long id = UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
                System.out.println("ID user1");
                in1 = new Scanner(System.in);
                Long id1 = in1.nextLong();
                System.out.println("ID user2");
                in1 = new Scanner(System.in);
                Long id2 = in1.nextLong();
                FriendRequest friendRequest = new FriendRequest((User)serviceUser.findOneUser(id1),(User)serviceUser.findOneUser(id2),"pending");
                friendRequest.setId(id);
                try {
                    Entity e =serviceFriendRequests.saveFrienRequest(friendRequest);;
                    if (e == null)
                        System.out.println("S-a trimis invitatia");
                    else
                        System.out.println("Deja exista");
                } catch (ValidationException ex) {
                    System.out.println(ex);
                }
            }
            if(o==18){
                        System.out.println("Introduceti id ul cererii de prietenie pentru care modificati statusul");
                        in1 = new Scanner(System.in);
                        Long id1 = in1.nextLong();
                        System.out.println("Introduceti statusul");
                        in1 = new Scanner(System.in);
                        String st = in1.nextLine();
                        FriendRequest friendRequest = (FriendRequest) serviceFriendRequests.findOneFriendRequest(id1);
                        friendRequest.setStatus(st);
                        serviceFriendRequests.updateFriendRequest(friendRequest);

                    }
            System.out.println("Introduceti o optiune");
            in = new Scanner(System.in);
            o = in.nextInt();
        }



    }
    public List<String> stringToList(String to){
        List<String> listIdUser = new ArrayList<String>(Arrays.asList(to.split(";")));
        return listIdUser;
    }

    public List<User> getToUserList(String list){
        List<String> listId = stringToList(list);
        List<User> listUser = new ArrayList<>();
        for(String s:listId){
            listUser.add((User)serviceUser.findOneUser(Long.parseLong(s)));
        }
        return listUser;
    }
}
