package ro.ubbcluj.map.domain;

import java.util.*;

public class User extends Entity<Long> {
    private final String firstName;
    private final String lastName;
    private List<User> friendList=new LinkedList<User>();;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public List<User> getFriendList() {
        return friendList;
    }

    public void addFriend(User friend) {
        this.friendList.add(friend);
    }

    public void removeFriend(User friend){
        this.friendList.remove(friend);
    }

    public void setFriendList(List<User> l){
        this.friendList=l;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                " first name=" + firstName +
                " last name=" + lastName +
                //" friend list=" + friendList+
                '}';

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }
}
