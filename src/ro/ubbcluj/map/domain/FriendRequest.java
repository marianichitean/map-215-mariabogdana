package ro.ubbcluj.map.domain;

public class FriendRequest extends Entity<Long>{
    User user1;
    User user2;
    String status;

    public FriendRequest(User user1, User user2, String status) {
        this.user1 = user1;
        this.user2 = user2;
        this.status = status;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}