package ro.ubbcluj.map.domain;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Objects;

public class Friendship extends Entity<Tuple<Long,Long>>{
    Timestamp date;
    private User u1;
    private User u2;

    public Friendship(User u1, User u2) {
        this.u1 = u1;
        this.u2 = u2;
    }

    public User getU1() {
        return u1;
    }

    public void setU1(User u1) {
        this.u1 = u1;
    }

    public User getU2() {
        return u2;
    }

    public void setU2(User u2) {
        this.u2 = u2;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "date=" + date +
                ", u1=" + u1 +
                ", u2=" + u2 +
                '}';
    }

    /**
     *
     * @return the date when the friendship was created
     */




    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Friendship that = (Friendship) o;
        return (Objects.equals(u1, that.u1) && Objects.equals(u2, that.u2)) || (Objects.equals(u1, that.u2) && Objects.equals(u2, that.u1));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), date, u1, u2);
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Timestamp getDate() {
        return date;
    }
}
