package ro.ubbcluj.map.model.validators;

public class ValidationExceptionF extends RuntimeException{

    public ValidationExceptionF(String message) {
        super(message);
    }

}
