package ro.ubbcluj.map.model.validators;
import ro.ubbcluj.map.domain.FriendRequest;
//import ro.ubbcluj.map.domain.Message;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.model.validators.Validator;
public class FriendRequestValidator implements Validator<FriendRequest> {

    @Override
    public void validate(FriendRequest entity) throws ValidationException {
        String erori = "";
        if(entity.getUser1() == null)
            erori = erori + "Nu exista utilizatorul cu id ul dat pentru primul user";
        if(entity.getUser2() == null)
            erori = erori + "Nu exista toti utilizatorii cu id urile date pentru al doilea";
        if(!erori.equals(""))
            throw new ValidationException(erori);
    }
}