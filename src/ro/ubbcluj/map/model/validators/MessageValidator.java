package ro.ubbcluj.map.model.validators;

import ro.ubbcluj.map.domain.Message;

public class MessageValidator implements Validator<Message>{

    @Override
    public void validate(Message entity) throws ValidationException {
        String erori = "";
        if(entity.getFrom() == null)
            erori = erori + "Nu exista utilizatorul cu id ul dat pentru expeditor";
        if(entity.getTo().contains(null))
            erori = erori + "Nu exista toti utilizatorii cu id urile date pentru destinatar";
        if(entity.getMessage().equals(""))
            erori = erori + "Mesajul nu poate sa fie null";
        //if(entity.getReplyMessage() == null)
          //  erori = erori + "Nu exista mesajul cu id ul dat la care doriti sa dati reply";
        if(!erori.equals(""))
            throw new ValidationException(erori);
    }
}
