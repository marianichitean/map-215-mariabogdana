package ro.ubbcluj.map.model.validators;

import ro.ubbcluj.map.domain.User;

public class LongValidator{

    public void validate(String s) throws ValidationException {
        if(s==null)
            throw new ValidationException("Nu trebuie sa fie nul");
        try{
            Long id=Long.parseLong(s);
        }
        catch (NumberFormatException e){
            throw new ValidationException("Nu este long ceea ce ai introdus");
        }
    }

}
