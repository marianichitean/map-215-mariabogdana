package ro.ubbcluj.map.model.validators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputValidator {

    public void validate(String s) throws ValidationException {
        String erori = "";
        if(!s.contains(";"))
            erori = erori + "Nu ati respectat formatul precizat";
        List<String> list = new ArrayList<String>(Arrays.asList(s.split(";")));
        for(String x:list) {
            try {
                Long.parseLong(x);
            } catch (NumberFormatException e) {
                 erori = erori + "ID urile sunt numere de tip Long";
            }
        }
        if(!erori.equals(""))
            throw new ValidationException(erori);

    }
}
