package ro.ubbcluj.map.model.validators;

import ro.ubbcluj.map.domain.Friendship;
import ro.ubbcluj.map.domain.User;

import java.util.List;
import java.util.Map;

public class FriendshipValidator implements Validator<Friendship> {

    @Override
    public void validate(Friendship entity) throws ValidationException {
        String errMsg="";
        if(entity.getU1().getId()==null || entity.getU2().getId()==null){
            errMsg=errMsg+"Id urile utilizatorilor nu trebuie sa fie nule ";
        }
        if(entity.getU1().getFirstName()=="" || entity.getU2().getFirstName()==""){
            errMsg=errMsg+"Numele utilizatorilor nu trebuie sa fie nule ";
        }
        if(entity.getU1().getLastName()=="" || entity.getU2().getLastName()==""){
            errMsg=errMsg+"Prenumele utilizatorilor nu trebuie sa fie nule";
        }
        if(errMsg!=""){
            throw new ValidationException(errMsg);
        }
    }

    public void validate1(Friendship entity, Iterable<User> list) throws ValidationExceptionF{

        String errMsg="";
        int ok1=0,ok2=0;

        for(User u:list){
            if(u.getId().equals(entity.getU1().getId())){
                ok1=1;
            }
            if(u.getId().equals(entity.getU2().getId())){
                ok2=1;
            }
        }
        if(ok1==0){
            errMsg=errMsg+"Nu exista primul utilizator introdus ";
        }

        if(ok2==0){
            errMsg=errMsg+"Nu exista al doilea utilizator introdus";
        }
        if(!(errMsg.equals(""))){
            throw new ValidationException(errMsg);
        }
    }
}