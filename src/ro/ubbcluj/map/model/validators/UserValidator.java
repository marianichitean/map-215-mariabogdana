package ro.ubbcluj.map.model.validators;

import ro.ubbcluj.map.domain.User;

public class UserValidator implements Validator<User> {

    @Override
    public void validate(User entity) throws ValidationException {
        String errMsg="";
        if(entity.getId()==null)
            errMsg=errMsg+"Id-ul nu poate fi null ";
        if(entity.getFirstName()=="")
            errMsg=errMsg+"Numele nu poate fi null ";
        if(entity.getLastName()=="")
            errMsg=errMsg+"Prenumele nu poate fi null";
        if(!errMsg.equals("")){
            throw new ValidationException(errMsg);
        }

    }
}
